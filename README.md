PROG 8110 – Lab 01
Due on 17th Sept 2019

Task:
In this lab, you will write a complete program in Java that computes the cost of a specialty
coffee or tea beverage. Your program is expected to prompt the user for input and validate it before computing the results. Your program should make use of expressions, decisions and basic input/output in Java.

Functional Specifications:
1. The program will prompt the user for various pieces of information about the desired beverage.
The information required is described below; some of the information is dependent on the type of beverage ordered. Once all the information has been entered, the program will compute and
display the amount of money charged for that customer’s beverage.

The program will prompt the user to enter the following (in the specified order):
a. The customer’s name (a string);
b. The type of beverage (coffee or tea) (a string);
c. The size of the beverage (small, medium, large) (a string).
d. Any added flavorings:
•	For coffee this could be none, vanilla or chocolate.
•	For tea this could be none, lemon or mint.
It will then process that customer’s information and display the results.

2. The input from the user should be as follows:
a. The customer’s name
b. Type of beverage (coffee or tea):
•	For coffee: “C”, “c”, “coffee” or ANY combination of upper- and lower-case letters
that correctly spells “coffee”, i.e., “Coffee”, “COFFEE”, “coffEE” is acceptable;
“Café” is NOT.
•	For tea: “T”, “t”, “tea” or ANY combination of upper- and lower-case letters that
correctly spells “tea”, i.e., “Tea”, “tEA” is acceptable; “Te” and “TEE” are NOT.

c. Size of the beverage: small (“small”, “S”, “s”), medium (“medium”, “M”, “m”), large
(“large”, “L”, “l”) or ANY combination of upper- and lower-case letters that correctly spells
“small”, “medium” or “large”.

d. Flavoring:
•	For none: The word “none” or ANY combination of upper- and lower-case letters that correctly spells “none”.
•	For vanilla: “V”, “v”, “vanilla” or ANY combination of upper- and lower-case letters
that correctly spells “vanilla”.
•	For chocolate: “C”, “c”, “chocolate” or ANY combination of upper and lower case
letters that correctly spells “chocolate”.
•	For lemon: “L”, “l”, “lemon” or ANY combination of upper- and lower-case letters
that correctly spells “lemon”.
•	For mint: “M”, “m”, “mint” or ANY combination of upper- and lower-case letters that
correctly spells “mint”.
3. The program will compute the amount of money that the customer owes for the beverage; the cost depends on the size of the beverage and any additional flavorings (NOTE: the customer gets a choice of only one flavoring!). The costs are as follows:
a. Size of beverage:
i. Small: $1.50
ii. Medium: $2.50
iii. Large: $3.25
b. Coffee flavorings:
i. Vanilla: $0.25
ii. Chocolate: $0.75
c. Tea flavorings:
i. Lemon: $0.25
ii. Mint: $0.50
4. The program should compute the total cost of the beverage with additional taxes; taxes are 13%. The final cost, with tax included, should be rounded to the nearest cent. For example, a small coffee with no flavoring would cost the customer a total of: $1.67 (1.50 * 1.11 = 1.665 which rounded to the nearest cent is 1.67 (let’s assume that we still have pennies!). (Note: make sure that the amount of money billed is displayed with a dollar sign and two decimal points).
5. The Program should work for 5 customers.
6. For a customer, the program should display a single line of output describing the beverage and the cost, e.g.:
For Peter, a medium tea, no flavoring, cost: $2.83.
All output should be appropriately labeled and formatted. The amount of money billed should
be displayed with a dollar sign and will be rounded to two fractional digits (for example, $25.99 or $67.87).

7. The program should also detect and report invalid input. When an invalid input is detected, the program will display an error message indicating the error, e.g., an invalid size specified. After displaying this information, the program should end without prompting for any additional information.

Examples of invalid input include entering a size or type of beverage that does not exist. Another example is entering a flavoring that does not exist for that beverage. For instance, if a user is getting coffee and enters MINT as the flavor of choice, your program should recognize this as an invalid input.

What to submit?
You need to submit the java application package in a zip format on e-Conestoga.
