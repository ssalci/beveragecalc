import java.math.BigDecimal;
import java.util.Arrays;




public class Order {
    String DrinkType = "";
    String DrinkSelection = "";
    String DrinkSize = "";
    String CustName = "";
    String Flavor = "";

    BigDecimal Cost = BigDecimal.valueOf(0);
    BigDecimal Small = BigDecimal.valueOf(1.5);
    BigDecimal Medium = BigDecimal.valueOf(2.50);
    BigDecimal Large = new BigDecimal(3.50);
    BigDecimal Vanilla = BigDecimal.valueOf(0.25);
    BigDecimal Chacolate = BigDecimal.valueOf(0.75);
    BigDecimal Lemon = BigDecimal.valueOf(0.25);
    BigDecimal Mint = BigDecimal.valueOf(0.50);

    String[] validTeaDrinks = {"tea", "t"};
    String[] validCofeeDrinks = {"coffee", "c"};

    String[] validSizes = {"s", "small", "m", "medium", "l", "large"};
    String[] validTeaFlavors = {"none", "n", "mint", "m", "lemon", "l"};
    String[] validCoffeeFlavors = {"none", "n", "vanilla", "v", "chocolate", "c"};
   // String[] validFlavors = {"none", "n", "vanilla", "v", "chocolate", "c", "n", "mint", "m", "lemon", "l"};


    public boolean validateBeverageType() {
        if (Arrays.asList(this.validTeaDrinks).contains(this.DrinkSelection.toLowerCase())) {
            this.DrinkType = "Tea";
            return true;
        } else if (Arrays.asList(this.validCofeeDrinks).contains(this.DrinkSelection.toLowerCase())) {
            this.DrinkType = "Coffee";
            return true;
        } else
            return false;
    }

    public boolean validateSize() {
        if (Arrays.asList(this.validSizes).contains(this.DrinkSize.toLowerCase())) {
            switch (this.DrinkSize.toLowerCase()) {
                case "s":
                case "small":
                    this.DrinkSize = "Small";
                    this.Cost = this.Cost.add(Small);
                    break;
                case "m":
                case "medium":
                    this.DrinkSize = "Medium";
                    this.Cost = this.Cost.add(Medium);
                    break;
                case "l":
                case "large":
                    this.DrinkSize = "Large";
                    this.Cost = this.Cost.add(Large);
                    break;
            }
            return true;
        } else
            return false;
    }

    public boolean validateTeaFlavor() {
        if (Arrays.asList(this.validTeaFlavors).contains(this.Flavor.toLowerCase())) {
            switch (this.Flavor.toLowerCase()) {
                case "n":
                    this.Flavor = "none";
                    break;
                case "m":
                    this.Flavor = "mint";
                    this.Cost = this.Cost.add(Mint);
                    break;
                case "l":
                    this.Flavor = "lemon";
                    this.Cost = this.Cost.add(Lemon);
                    break;

            }
            return true;
        } else
            return false;
    }

    public boolean validateCoffeeFlavor() {
        if (Arrays.asList(this.validCoffeeFlavors).contains(this.Flavor.toLowerCase())) {
            switch (this.Flavor.toLowerCase()) {
                case "n":
                    this.Flavor = "none";
                    break;
                case "v":
                    this.Flavor = "vanilla";
                    this.Cost = this.Cost.add(Vanilla);
                    break;
                case "c":
                    this.Flavor = "chocolate";
                    this.Cost = this.Cost.add(Chacolate);
                    break;
            }
            return true;
        } else
            return false;
    }

}